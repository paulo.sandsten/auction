import axios from "axios";
import { AuctionItem, Bid } from "../common/types";

const BASE =
  process.env.NODE_ENV === "development"
    ? "http://localhost:5000/api"
    : "https://e741-5-148-5-130.ngrok-free.app/api";

const baseApiUrls = {
  items: `${BASE}/items`,
  item: (id: string) => `${BASE}/items/${id}`,
  itemBid: (id: string) => `${BASE}/items/${id}/bid`,
  itemBids: (id: string) => `${BASE}/items/${id}/bids`,
  images: (fileName: string) => `${BASE}/images/${fileName}`,
  imageLow: (fileName: string) => `${BASE}/images/lowres/${fileName}`,
  imageHigh: (fileName: string) => `${BASE}/images/highres${fileName}`,
};

export let apiUrls =
  process.env.NODE_ENV === "development"
    ? {
        ...baseApiUrls,
        // MSW mocks will return a full url.
        images: (fileName: string) => fileName,
        imageLow: (fileName: string) => fileName,
        imageHigh: (fileName: string) => fileName,
      }
    : baseApiUrls;

export const api = () => {
  const getAuctionItems = async () =>
    axios.get(apiUrls.items).then((res) => res.data);

  const getAuctionItem = async (id: string) =>
    axios.get(apiUrls.item(id)).then((res) => res.data);

  const getAuctionItemBids = async (id: string) =>
    axios.get(apiUrls.itemBids(id)).then((res) => res.data);

  const createAuctionItem = async (payload: AuctionItem) =>
    axios.post(apiUrls.items, payload).then((res) => res.data);

  // const createAuctionItemBid = async (payload: Bid) =>
  //   axios.post(apiUrls.itemBid(payload), payload).then((res) => res.data);

  return {
    getAuctionItems,
    getAuctionItem,
    getAuctionItemBids,
    createAuctionItem,
  };
};

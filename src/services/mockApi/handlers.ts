import { rest } from "msw";
import { faker } from "@faker-js/faker";
import { Bid } from "@types";

const MOCK_ITEMS = 20;

const mockedBid: Bid = {
  amount: faker.number.int(1000),
  bidder: faker.person.fullName(),
  time: Date.now() - faker.number.int({ min: 100, max: 10_000 }),
};

const createDate = () =>
  new Date(
    Date.now() - faker.number.int({ min: 100, max: 10_000 })
  ).toISOString();

const createAuctionItem = () => ({
  id: faker.string.uuid(),
  name: faker.commerce.productName(),
  description: faker.commerce.productDescription(),
  // Server responded with this data instead: 2023-07-26T16:00:00.000Z
  // The type provided needs change.
  startTime: createDate() as unknown as number,
  duration: faker.number.int({ min: 5, max: 60 * 2 }), // will be "seconds"
  currentBid: mockedBid.amount,
  bids: [mockedBid],
  image: faker.image.url(),
});

const createMockedAuctionItems = (n: number) => {
  return [...Array(n)].map(() => createAuctionItem());
};

const mockedAuctionItems = createMockedAuctionItems(MOCK_ITEMS);

export const handlers = [
  rest.get("/api/items", (_req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockedAuctionItems));
  }),
];

import { useState } from "react";
import { useQuery } from "react-query";
import { api } from "@services";
import { AuctionItem } from "@types";
import { AuctionItemCard } from "@components";
import { isAuctionItemOpen } from "@utils";
import styles from "./Home.module.css";

const POLLING_TIME = 15_000;

export const Home = () => {
  // Polling
  const { isLoading, error, data } = useQuery(
    "AuctionItems",
    api().getAuctionItems,
    {
      refetchInterval: POLLING_TIME,
      refetchIntervalInBackground: true,
    }
  );
  const [filteredItems, setFilteredItems] = useState<AuctionItem[]>([]);

  const handleCreateAuctionItem = () => {
    console.log("we can open a modal here with a form");
  };

  const handleBid = (id: string) => {
    console.log("We can open up a modal here...", id);
  };

  const handleDetails = (id: string) => {
    console.log("We can open a side panel here or perhaps a new view", id);
  };

  const handleFilterOpenAuctionItems = () => {
    setFilteredItems([
      ...data?.filter((item: AuctionItem) => isAuctionItemOpen(item)),
    ]);
  };

  const handleClearFilter = () => {
    setFilteredItems(data);
  };

  if (error) return "error...";
  if (isLoading) return "loading...";

  const itemLength = (a: any[]) => {
    return a.length;
  };

  return (
    <div>
      <h1 className={styles.heading}>
        Auction Items:{" "}
        {itemLength(filteredItems.length > 0 ? filteredItems : data)}
      </h1>

      <div className={styles.actions}>
        <button onClick={handleFilterOpenAuctionItems}>Open Items</button>
        <button onClick={handleClearFilter}>All Items</button>
        <button onClick={handleCreateAuctionItem}>New Auction Item</button>
      </div>

      <ul className={styles.list}>
        {(filteredItems.length > 0 ? filteredItems : data).map(
          (item: AuctionItem) => (
            <li key={item.id}>
              <AuctionItemCard
                item={item}
                handleBid={handleBid}
                handleDetails={handleDetails}
              />
            </li>
          )
        )}
      </ul>
    </div>
  );
};

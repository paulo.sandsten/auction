import { AuctionItem } from "@types";
import { apiUrls } from "@services";
import { isAuctionItemOpen, auctionItemCountdown } from "@utils";
import styles from "./AuctionItemCard.module.css";

interface AuctionItemCardProps {
  item: AuctionItem;
  handleBid: (auctionItemId: string) => void;
  handleDetails: (auctionItemId: string) => void;
}

export const AuctionItemCard: React.FC<AuctionItemCardProps> = ({
  item,
  handleBid,
  handleDetails,
}) => {
  const isOpen = isAuctionItemOpen(item);
  // There can be a second countdown here. I made a quick review just to get a minimal feature out.
  // This requires a bit more engineering as setTimeouts are a cause of memory leaks, and honestly. I just have my lunch time :)
  const { minutes } = auctionItemCountdown(item);

  return (
    <>
      <div className={styles.imageWrapper}>
        <img
          alt=""
          loading="lazy"
          srcSet={`${apiUrls.imageLow(
            item.image ?? ""
          )} 480w, ${apiUrls.imageHigh(item.image ?? "")} 800w`}
          sizes="(max-width: 600px) 480px,
                   800px"
          src={apiUrls.imageHigh(item.image ?? "")}
        />
      </div>
      <p>{item.name}</p>
      <p>{item.description}</p>
      <p>{minutes < 0 ? "Expired" : `${minutes} minutes left`}</p>
      <p>
        {isOpen ? "Current bid" : "Sold for"} ${item.currentBid}
      </p>
      <button disabled={!isOpen} onClick={() => handleBid(item.id as string)}>
        {isOpen ? "Bid" : "Closed"}
      </button>
      <button onClick={() => handleDetails(item.id as string)}>Details</button>
    </>
  );
};

import { isAuctionItemOpen } from ".";
import { AuctionItem } from "../types";

describe("isAuctionItemOpen", () => {
  it("isAuctionItemOpen should return false", () => {
    const input = {
      startTime: "2023-06-26T16:00:00.000Z",
      duration: 10800,
    };
    const output = false;

    expect(isAuctionItemOpen(input as AuctionItem)).toBe(output);
  });

  it("isAuctionItemOpen should return true", () => {
    const input = {
      startTime: new Date().toISOString(),
      duration: 10800,
    };
    const output = true;

    expect(isAuctionItemOpen(input as AuctionItem)).toBe(output);
  });
});

import { AuctionItem } from "../types";

export const isAuctionItemOpen = (item: AuctionItem) =>
  !!(new Date(item.startTime).getTime() + item.duration * 1000 > Date.now());

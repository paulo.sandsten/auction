import { AuctionItem } from "../types";

export const auctionItemCountdown = (item: AuctionItem) => {
  const now = Date.now();
  const startTime = new Date(item.startTime).getTime();
  const countdown = startTime + item.duration * 1000;
  const timeLeft = countdown - now;

  const minutes = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((timeLeft % (1000 * 60)) / 1000);

  return {
    minutes,
    seconds,
    open: !!(timeLeft > 0),
  };
};

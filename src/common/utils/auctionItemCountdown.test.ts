import { auctionItemCountdown } from ".";
import { AuctionItem } from "../types";

const mockedInput = {
  startTime: "2023-06-26T16:00:00.000Z",
  duration: 10800,
};

describe("auctionItemCountdown", () => {
  it("auctionItemCountdown should return return minutes, seconds, and open properties", () => {
    expect(auctionItemCountdown(mockedInput as AuctionItem)).toEqual(
      expect.objectContaining({
        minutes: expect.any(Number),
        seconds: expect.any(Number),
        open: expect.any(Boolean),
      })
    );
  });

  it("auctionItemCountdown should return false as auction item expired", () => {
    const { open } = auctionItemCountdown(mockedInput as AuctionItem);
    const output = false;
    expect(open).toEqual(output);
  });
});

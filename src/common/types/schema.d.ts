export interface components {
  schemas: {
    Bid: {
      amount: number;
      bidder: string;
      /**
       * Format: int64
       * @description The time when the bid was placed, expressed as Unix epoch in milliseconds.
       */
      time: number;
    };
    AuctionItem: {
      id: string;
      name: string;
      description: string;
      /**
       * Format: string
       * @description The auction start time, expressed with ISO date string
       */
      startTime: string;
      /**
       * Format: int32
       * @description The duration of the auction in seconds.
       */
      duration: number;
      currentBid: number | null;
      bids: components["schemas"]["Bid"][];
      /** @description The name of the image file for the item. To access the image, use the path '/images/lowres/[image-file-name]' for low resolution, and '/images/highres/[image-file-name]' for high resolution. */
      image: string;
    };
  };
  responses: never;
  parameters: never;
  requestBodies: never;
  headers: never;
  pathItems: never;
}

export type external = Record<string, never>;

export type operations = Record<string, never>;

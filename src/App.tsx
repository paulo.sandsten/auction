import { Home } from "@components";
import styles from "./App.module.css";

function App() {
  return (
    <div className={styles.app}>
      <Home />
    </div>
  );
}

export default App;

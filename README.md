## Install

```bash
pnpm i
```

## Running the development server

```bash
pnpm run dev
```

## Run against your own ngrok

There will need to be a change in the `api.ts` file where you specify the path as the process env variable will be set to "development"

## Notes

- Changed the type manually as the data coming back didn't match the spec (We are getting the ISO string):

```yaml
startTime:
  type: "integer"
  format: "int64"
  description: "The auction start time, expressed as Unix epoch in milliseconds."
```

- Barebones/Minimal CSS just to structure it, not to style it pretty.
- Test are working as I included config and more packages to the project.
- Added MSW to mock server requests.
- For a real project. Every layer needs it's tests. Here I am just testing two utils.
- TS might barf at you as the aliases were set up with a different config.

## A second branch

I started a second branch just to clean up, style a little more, and address UI bugs. Check it out, `extra-time`.